# word-racer-Raj

URL - Word Race (word-race-react.herokuapp.com)

After reading the Assignment Document , i got the idea that i had to build 2 GET APi’s i.e., to get all the user data, to get top 10 user leaderboard and 1 POST API to send name, email, score and level to the server.

I started with building the demo API first and checked if the setup is working fine. Then While building the real API, I created a Database on the MongoDB atlas server so that i can host the backend in future i.e, Server side code and for API I created the player model having name, score, level, email etc. And then proceeded with the controllers ie, for sending and getting request and response from the server.

Then I configured the route for all the controllers and handled the errors and exceptions all over the application at last.

After the backend (server side code) got over , I started building designs on Figma for the UI. When designs were ready , I started building the frontend starting with the instruction page which is the root of this application.

Then I designed the mainGame page of the application. I used setTimeout in a recursive way inside a function to pop or display the words on the screen. If the user types the highlighted word (blue in border-color) on the stack within the time interval of that word, then the word will disappear from the word stack with a beep sound. And if the user types wrong text then a bomb explosion sound will be heard and the user is given another chance to type the word but within that given interval of time only.

When the user misses the word or is not able to type correctly within the time interval of the word, the word gets added to the Wordstack (with a red border- color). And the game gets over when the Word stack becomes full , currently it gets full in 6 words.

The interval of a word in which the user can type and clear decreases when the multiplier increases and resets on mistype. And after a fixed amount of increases the interval is set to 1 second , because it's not practically possible to type the words if the interval decreases furthermore.
And when the Word Stack becomes full , then the game is automatically redirected into another page ie., retryGamePage, Where the user can play the game again(where is reset the game to previous state i.e., score 0 level 0 ), and the user can also save the current game's score here , also users can see the scores of all users and the top 10 winners leaderboard in this page.
So in this page I am redirectly the user to other pages where I consume the API’s I have built in the server side.

The final work was to implement and check the routing of the pages properly. Then I deployed both the Frontend and Backend in heroku for you to check.
